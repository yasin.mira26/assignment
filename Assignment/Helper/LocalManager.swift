//
//  LocalManager.swift
//  BantuJiwa
//
//  Created by Mohammad yasin on 03/03/21.
//

import UIKit
import SwiftyJSON

class LocalManager: NSObject {
    class func saveMember(sender:Data){
        let userDefaults = UserDefaults.standard
        userDefaults.setValue(sender, forKey: "MemberModel")
        userDefaults.synchronize()
    }
    
//    class func getMember()->MemberModel? {
//        guard let data = UserDefaults.standard.value(forKey: "MemberModel") as? Data else { return nil}
//        let json = JSON(data)
//        return MemberModel.init(fromJson: json)
//    }
//
    class func removeMember(){
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: "MemberModel")
        userDefaults.synchronize()
    }
}
