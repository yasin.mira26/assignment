//
//  ApiManager.swift
//  BantuJiwa
//
//  Created by Mohammad yasin on 03/03/21.
//

import UIKit
import Alamofire
import SwiftyJSON
import RxSwift

class ApiManager: NSObject {
    public static let BASE_URL = "https://dashboard.bantujiwa.com/api/"
    let loginSosmed = "login"
    let provinsi = "provinsi"
    let kota = "kota"
    let jobList = "job-list"
    let identity = "identity"
    let assesment = "assesment"
    let card = "card"
    let dashboard = "dashboard"
    let artikel = "article"
    let artikelDetail = "article"
    
    static let instance = ApiManager()
    let manager: APISessionManager
    let dateFormatter: DateFormatter
    
    override init() {
        dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        manager = APISessionManager(configuration: configuration)
        super.init()
    }
    
//    func postRegister(param:Parameters) -> Observable<APIResponse> {
//        let url = ApiManager.BASE_URL+ApiManager.REGISTER_URL
//        let request = manager.request( url, method: .post, parameters: param, encoding: URLEncoding.default)
//        return request.rx_JSON()
//            .mapJSONResponse()
//            .map { response in
//                if response.code == 200 {
//                    return response
//                }
//                throw errorWithCode(code: .GlobalError(code: response.code, reason: response.message))
//        }
//    }
//
//    func Login(Username:String,Password:String) -> Observable<MemberModel> {
//        let url = ApiManager.BASE_URL+LOGIN_URL
//        let request = manager.request( url, method: .post, parameters: ["member_username":Username,"member_password":Password], encoding: URLEncoding.default)
//        return request.rx_JSON()
//            .mapJSONResponse()
//            .map { response in
//                if response.code == 200 {
//                    let model = MemberModel.init(fromJson: response.result)
//                    model.password = Password
//                    var json = JSON(response.result)
//                    json["password"].stringValue = Password
//                    guard let rawData = try? json.rawData() else { return model}
//                    LocalManager.saveMember(sender: rawData)
//                    return model
//                }
//                throw errorWithCode(code: .GlobalError(code: response.code, reason: response.message))
//        }
//    }
//
//    func getPromoList() -> Observable<[PromoModel]> {
//        let url = ApiManager.BASE_URL+PROMO_LIST_URL
//        let request = manager.request( url, method: .get, parameters: [:], encoding: URLEncoding.default)
//        return request.rx_JSON()
//            .mapJSONResponse()
//            .map { response in
//                if response.result != nil {
//                    var list = [PromoModel]()
//                    for data in response.result.arrayValue {
//                        let model = PromoModel.init(fromJson: data)
//                        list.append(model)
//                    }
//                    return list
//                }
//                throw errorWithCode(code: .GlobalError(code: response.code, reason: response.message))
//        }
//    }
//
//    func getPromoDetail(promoId:String) -> Observable<PromoDetailModel> {
//        let url = ApiManager.BASE_URL+PROMO_DETAIL_URL
//        let request = manager.request( url, method: .post, parameters: ["promo_id":promoId], encoding: URLEncoding.default)
//        return request.rx_JSON()
//            .mapJSONResponse()
//            .map { response in
//                if response.code == 200 {
//                    let model = PromoDetailModel.init(fromJson: response.result)
//                    return model
//                }
//                throw errorWithCode(code: .GlobalError(code: response.code, reason: response.message))
//        }
//    }
}

extension Observable {
    func mapJSONResponse() -> Observable<APIResponse> {
        return map { (item: Element) -> APIResponse in
            guard let json = item as? JSON else {
                return APIResponse(code: -1, message: "JSON ERROR. Not In JSON Format", result: JSON())
            }
            var code = 0;
            var message = "";
            var result = json;
            if json["STATUS"].exists(){
                code = json["STATUS"].intValue
            }
            if json["STATUS_CODE"].exists(){
                code = json["STATUS_CODE"].intValue
            }
            if json["MESSAGE"].exists(){
                message = json["MESSAGE"].stringValue
            }
            if json["DATA"].exists(){
                result = json["DATA"]
            }
            //print("response = \(json)")
            return APIResponse(code: code, message: message, result: result)
        }
    }
}

struct APIResponse {
    var code: Int
    var message: String
    var result: JSON
    
    init(code: Int, message: String, result: JSON) {
        self.code = code
        self.message = message
        self.result = result
    }
}

class APISessionManager: SessionManager {
    
    internal override func request(_ url: URLConvertible, method: HTTPMethod, parameters: Parameters?, encoding: ParameterEncoding = JSONEncoding.default, headers: HTTPHeaders? = nil) -> DataRequest {
        var overridedParameters = [String : AnyObject]()
        print("ovverided parameters = \(overridedParameters)")
        if let parameters = parameters {
            overridedParameters = parameters as [String : AnyObject]
        }
        
        var overridedHeaders = [String: String]()
        // overridedHeaders["devel"] = "bugs"
        let userAgent = UIDevice.current.name + "; " + UIDevice.current.systemName + " " + UIDevice.current.systemVersion
        overridedHeaders["User-Agent"] = userAgent
//        overridedHeaders["APP_TOKEN"] = ApiManager.APP_TOKEN
//        let member = LocalManager.getMember()
//        if member?.userId != "" && member?.userId != nil{
//            overridedHeaders["USER_TOKEN"] = member?.token
//            if method == .post {
//                overridedParameters["MEMBER_ID"] = member?.userId as AnyObject
//                overridedParameters["member_id"] = member?.userId as AnyObject
//            }
//        }else{
//            overridedHeaders["USER_TOKEN"] = ""
//            overridedParameters["MEMBER_ID"] = "" as AnyObject
//        }
        
        overridedHeaders["X-Platform"] = "ios"
        
        if let info = Bundle.main.infoDictionary, let version = info["CFBundleShortVersionString"] as? String {
            overridedHeaders["app-version"] = version
            overridedHeaders["X-Client-Version"] = version
            
        }
        //        if let token = PreferenceManager.instance.token, token.isEmpty == false{
        //            overridedHeaders["Authorization"] = ("Bearer " + token)
        //        }
        if let headers = headers {
            for (key, value) in headers {
                overridedHeaders[key] = value
            }
        }
        
        #if DEBUG
        print("param: ",overridedParameters)
        print("header: ",overridedHeaders)
        #endif
        
        return super.request(url, method: method, parameters: overridedParameters, encoding: encoding, headers: overridedHeaders)
    }
}

extension DataRequest{
    
    func rx_JSON(options: JSONSerialization.ReadingOptions = .allowFragments) -> Observable<JSON> {
        let observable = Observable<JSON>.create { observer in
            if let method = self.request?.httpMethod, let urlString = self.request?.url {
                print("[\(method)] \(urlString)")
                if let body = self.request?.httpBody {
                    print(NSString(data: body, encoding: String.Encoding.utf8.rawValue) as Any)
                }
            }
            
            self.responseJSON(options: options) { response in
                if let error = response.result.error {
                    let string = String(data: response.data!, encoding: String.Encoding.utf8)
                    print(string as Any)
                    observer.onError(error)
                } else if let value = response.result.value {
                    let json = JSON(value)
                    if let error = json.error {
                        observer.onError(error)
                    } else {
                        observer.onNext(json)
                        observer.onCompleted()
                    }
                    
                } else {
                    observer.onError(errorWithCode(code: .UnknownError))
                }
            }
            return Disposables.create(with: self.cancel)
        }
        return Observable.deferred { return observable }
    }
}
