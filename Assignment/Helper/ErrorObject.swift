//
//  ErrorObject.swift
//  BantuJiwa
//
//  Created by Mohammad yasin on 03/03/21.
//

import UIKit

class ErrorObject: NSObject {

}
let ErrorObjectDomain = "BayuBuanaErrorDomain"

enum PVGErrorCode {
    case UnknownError
    case JSONParserError
    case GlobalError(code: Int, reason: String)
}

func errorWithCode(code: PVGErrorCode) -> NSError {
    switch code {
    case .UnknownError:
        return NSError(domain: ErrorObjectDomain, code: -1, userInfo: [NSLocalizedDescriptionKey: "Unknown Error"])
    case .JSONParserError:
        return NSError(domain: ErrorObjectDomain, code: -2, userInfo: [NSLocalizedDescriptionKey: "JSON parser error"])
    case let .GlobalError(code, reason):
        return NSError(domain: ErrorObjectDomain, code: code, userInfo: [NSLocalizedDescriptionKey: reason])
    }
}
